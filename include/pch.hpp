#pragma once

#include <iostream>
#include <vector>
#include <intrin.h>

using uint32 = uint32_t;
using int32 = int32_t;
using uint16 = uint16_t;
using int16 = int16_t;
