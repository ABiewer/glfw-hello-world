#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;
out vec3 vertexPosition;

uniform vec3 offset;

void main()
{
    vertexPosition = aPos + offset;
    gl_Position = vec4(vertexPosition, 1.0);
    ourColor = aColor;
}